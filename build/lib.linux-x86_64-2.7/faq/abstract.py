# coding: utf-8
__author__ = 'luis.villanueva'
from datetime import datetime
from django.contrib import admin
from django.db import models


class Restricted(admin.ModelAdmin):
    actions = None
    mymodel = None

    def has_add_permission(self, request):
        assert self.mymodel is not None, "No set mymodel var"
        objects = getattr(self.mymodel, "all_objects", self.mymodel.objects)
        if objects.count() >= 1:
            return False
        else:
            return True

    def has_delete_permission(self, request, obj=None):
        return False


class DatedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class NoDeleteManager(models.Manager):
    """
    Manager para borrado logico de modelos.
    """
    def get_query_set(self):
        """
        Devuelve un query_set excluyendo los elementos borrados logicamente.

        @rtype: queryset
        @return: query_set excluyendo los elementos borrados logicamente.
        """
        query_set = super(NoDeleteManager, self).get_query_set()
        return query_set.filter(deleted_at__isnull=True)


class TrackableModel(DatedModel):
    """
    Modelo de fechas para poder rastrear eventos de usuarios en la aplicación.
    """
    deleted_at = models.DateTimeField(blank=True, null=True, editable=False)
    # las llaves compuestas que aceptan nulos no son tomadas en cuenta en MySql
    deleted_fl = models.CharField(max_length=30, default='0', editable=False)

    objects = NoDeleteManager()

    class Meta:
        abstract = True

    def delete(self):
        """
        Ajusta las fechas de liminación / modificación de un elemento.
        """

        hoy = datetime.now()
        self.deleted_at = hoy
        self.deleted_fl = str(hoy)
        self.save()

        seen_objs = models.query_utils.CollectedObjects()
        self._collect_sub_objects(seen_objs)
        for child_models in seen_objs.children.values():
            for child in child_models:
                instances = seen_objs.data.get(child, {})
                for ins in instances.values():
                    if ins != self and ins.fecha_eliminacion is None:
                        ins.delete()
