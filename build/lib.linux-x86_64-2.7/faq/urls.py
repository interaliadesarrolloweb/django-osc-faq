# coding: utf-8
from django.conf.urls.defaults import *

urlpatterns = patterns('faq.views',
    url(r'^$', 'topics_list', name='faq_topics_list'),
    url(r'^feedback/submit/$', 'submit_feedback', name="faq_submit_feedback"),
    url(r'^(?P<slug>[\w-]+)/$', 'topic_detail', name='faq_topic_detail'),
)