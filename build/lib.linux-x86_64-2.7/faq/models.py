# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin

from .abstract import DatedModel
from .managers import QuestionManager


class FAQConfig(DatedModel):
    num_columns = models.PositiveIntegerField(default=2)
    num_elements = models.PositiveIntegerField(default=8)

    def __unicode__(self):
        return u"Config faq: %s - %s" % (self.num_columns, self.num_elements)


class Topic(DatedModel):
    """
    Generic Topics for FAQ question grouping
    """
    name = models.CharField(_('name'), max_length=150)
    slug = models.SlugField(_('slug'), max_length=150, unique=True)
    active = models.BooleanField(default=True)
    order = models.PositiveIntegerField(
        _('sort order'), default=0,
        help_text=_('The order you would like the topic to be displayed.'))
    feedback_email = models.EmailField(
        _('Email to receive feedback'), blank=True, null=True)

    all_objects = models.Manager()
    objects = QuestionManager()

    class Meta:
        verbose_name = _("F.A.Q. Topic")
        verbose_name_plural = _("F.A.Q. Topics")
        ordering = ['order', 'created_at']

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('faq_topic_detail', [self.slug], {})


class Question(DatedModel):
    text = models.TextField(
        _('question'), help_text=_('The actual question.'))
    answer = models.TextField(
        _('answer'), help_text=_('The answer text.'))
    topic = models.ForeignKey(
        Topic, verbose_name=_('topic'), related_name='questions')

    active = models.BooleanField(default=True)
    order = models.PositiveIntegerField(
        _('sort order'), default=0,
        help_text=_('The order you would like the question to be displayed.'))

    all_objects = models.Manager()
    objects = QuestionManager()

    class Meta:
        verbose_name = _("Frequent asked question")
        verbose_name_plural = _("Frequently asked questions")
        ordering = ['order', 'created_at']

    def __unicode__(self):
        return self.text

    @models.permalink
    def get_absolute_url(self):
        return ('faq_question_detail', [self.topic.slug, self.slug], {})

    def positive_feedback(self):
        return self.feedback.filter(is_useful=QuestionFeedback.YES).count()

    def negative_feedback(self):
        return self.feedback.filter(is_useful=QuestionFeedback.NO).count()


class QuestionFeedback(DatedModel):
    # CONSTANTS ===============================================================
    YES = 'Yes'
    NO = 'No'
    USEFUL_CHOICES = (
        (YES, _("Yes, it was.")),
        (NO, _("Not really.")),
    )
    # FIELDS ==================================================================
    question = models.ForeignKey(Question, related_name="feedback")
    name = models.CharField(_("Your name"), max_length=100)
    email = models.EmailField(_("Your e-mail"))
    is_useful = models.CharField(
        _("Was this answer useful?"), max_length=3, choices=USEFUL_CHOICES)
    reason = models.TextField(
        _("Why?"), blank=True, null=True, default=_("Because..."))

    class Meta:
        verbose_name = _("User feedback")
        verbose_name_plural = _("Users feedback")
        ordering = ('created_at',)

    def __unicode__(self):
        return self.name


class FAQPlugin(CMSPlugin):
    topic = models.ForeignKey(Topic)
