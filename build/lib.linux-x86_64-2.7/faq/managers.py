# coding: utf-8
from django.db import models

class QuestionManager(models.Manager):
    def get_query_set(self):
        qs = super(QuestionManager, self).get_query_set()
        return qs.filter(active=True)
