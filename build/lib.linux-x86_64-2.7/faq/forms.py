# coding: utf-8
from django import forms
from django.utils.translation import ugettext as _
#from base.enums import SIMPLE_EDITOR_CONFIG
from tinymce.widgets import TinyMCE
from .models import QuestionFeedback, Question


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        widgets = {
            'answer_es': TinyMCE(
                attrs={'cols': 80, 'rows': 5},
            ),
            'answer_en': TinyMCE(
                attrs={'cols': 80, 'rows': 5},
            ),
        }


class QuestionFeedbackForm(forms.ModelForm):
    class Meta:
        model = QuestionFeedback
        exclude = ('created_at', 'updated_at')
        widgets = {
            'question': forms.HiddenInput(),
        }
    def clean(self):
        cleaned_data = self.cleaned_data
        is_useful = cleaned_data.get('is_useful')
        reason = cleaned_data.get('reason')

        if is_useful==QuestionFeedback.NO and (not reason or reason==_("Because...")):
            msg = _("Please tell us why this answer wasn't helpful to you.")
            self._errors['reason']=self.error_class([msg])

            try:
                del cleaned_data['reason']
            except KeyError:
                pass

        return cleaned_data

