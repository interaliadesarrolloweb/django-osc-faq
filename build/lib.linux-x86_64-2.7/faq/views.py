# coding: utf-8
from cms.utils.mail import send_mail
from django.http import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils import simplejson
from django.utils.translation import ugettext as _, get_language
from django.views.decorators.cache import cache_page

from .models import Topic, FAQConfig
from .forms import QuestionFeedbackForm
from .settings import FAQ_TOPICS_LIST_CACHE, \
    FAQ_TOPIC_DETAIL_CACHE
from .utils import FeedbackCounter


@cache_page(FAQ_TOPICS_LIST_CACHE)
def topics_list(request):
    topics = Topic.objects.all()

    try:
        config = FAQConfig.objects.get()
    except FAQConfig.DoesNotExist:
        config = None

    def group(lst, n):
        for i in range(0, len(lst), n):
            val = lst[i:i + n]
            yield tuple(val)

    if config:
        topics1 = group(topics, config.num_elements)
        topics2 = group(topics, config.num_elements)
        width = (100 / config.num_columns)
    else:
        topics1 = group(topics, 8)
        topics2 = group(topics, 8)
        width = (100 / 2)

    context = {
        'topics': topics1,
        'topics2': topics2,
        'config': config,
        'width': width,
    }
    return render(request, "faq/topic_list.html", context)


@cache_page(FAQ_TOPIC_DETAIL_CACHE)
def topic_detail(request, slug):
    lang = get_language()
    if lang[:2] == "en":
        topic = get_object_or_404(Topic, slug_en=slug)
    else:
        topic = get_object_or_404(Topic, slug_es=slug)

    counter = FeedbackCounter(request)
    context = {
        'topic': topic,
        'max_feedback_reached': counter.max_feedback_reached(),
    }
    return render(request, "faq/topic_detail.html", context)


def submit_feedback(request):
    if request.method != "POST":
        raise Http404
    form = QuestionFeedbackForm(request.POST)
    errors = []
    counter = FeedbackCounter(request)
    max_feedback_reached = counter.max_feedback_reached()
    if form.is_valid() and not max_feedback_reached:
        instance = form.save(commit=False)
        instance.save()
        counter.step()
        if instance.question.topic.feedback_email:
            send_mail(
                subject=_("F.A.Q. feedback"),
                to=(instance.question.topic.feedback_email,),
                txt_template='faq/mail/feedback_email.txt',
                html_template='faq/mail/feedback_email.html',
                context={'feedback': instance},
                fail_silently=True
            )
    else:
        errors = form._errors

    result = {
        'errors': errors,
        'id_question': request.POST.get('question'),
        'max_feedback_reached': max_feedback_reached,
    }
    json = simplejson.dumps(result)
    return HttpResponse(json, mimetype="application/json")
