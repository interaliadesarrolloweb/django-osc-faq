# coding: utf-8
from modeltranslation.translator import translator, TranslationOptions

from .models import Topic, Question


class TopicTrans(TranslationOptions):
    fields = ('name', 'slug')

translator.register(Topic, TopicTrans)


class QuestionTrans(TranslationOptions):
    fields = ('text', 'answer')

translator.register(Question, QuestionTrans)
