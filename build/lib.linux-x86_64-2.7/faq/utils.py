# coding: utf-8
__author__ = 'luis.villanueva'

import datetime
from .settings import FAQ_MAX_FEEDBACK

class FeedbackCounter(object):
    def __init__(self, request):
        self._request = request
        self.counter=request.session.get('faq_feedback_counter',0)
        self.last_feedback_date=request.session.get('faq_last_feedback_date')

    def max_feedback_reached(self):
        today = datetime.date.today()
        max_feedback_reached=False
        if self.last_feedback_date==today:
            if self.counter >= FAQ_MAX_FEEDBACK:
                max_feedback_reached=True
        else:
            self.counter=0
        return max_feedback_reached

    def step(self):
        today = datetime.date.today()

        self.counter+=1
        self._request.session['faq_last_feedback_date']=today
        self._request.session['faq_feedback_counter']=self.counter
        return self.counter