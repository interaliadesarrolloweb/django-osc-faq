OSC - FAQ
=========
by Interalia.net's Open Source Developers:
- jonykalavera
- zodman

Simple FAQ django app. Compatible with django-cms.

Requirements
============

django-tinymce>=1.5
django-modeltranslation>=0.5
