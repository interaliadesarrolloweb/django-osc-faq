# coding: utf-8
__author__ = 'luis.villanueva'

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class FAQApp(CMSApp):
    name = _("F.A.Q. App")
    urls = ["faq.urls"]

apphook_pool.register(FAQApp)
