# coding: utf-8
__author__ = 'jony kalavera'
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import FAQPlugin
from .utils import FeedbackCounter


class CMSFaqPlugin(CMSPluginBase):
    """
    Clase plugin para agregar listas de preguntas frecuentes.
    """
    model = FAQPlugin
    name = _(u"F.A.Q. Topic")
    render_template = "faq/topic_render.html"
    admin_preview = False

    def render(self, context, instance, placeholder):
        request = context.get('request')
        counter = FeedbackCounter(request)
        context = {
            'instance': instance,
            'placeholder': placeholder,
            'topic': instance.topic,
            'max_feedback_reached': counter.max_feedback_reached(),
            'MEDIA_URL': settings.MEDIA_URL,
        }
        return context

plugin_pool.register_plugin(CMSFaqPlugin)
