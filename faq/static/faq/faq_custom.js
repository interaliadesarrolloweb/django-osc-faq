/**
 * Created by PyCharm.
 * User: luis.villanueva
 * Date: 9/13/11
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
(function($){
    $(document).ready(function(){
        $('.feedback-toggle select').change(function(evt){
            console.log("faq","select");
            var form_div = $(this).parent().next();
            var form = $(this).parent().parent();
            if($(this).val()){
                form_div.show();
            } else {
                form_div.hide();
                form.get(0).reset();
            }
        });
        $('.faq_feedback_form button[type=reset]').click(function(){
            $(this).parent().hide();
        });
        $('.faq_feedback_form').each(function(){this.reset();});
        $('.faq_feedback_form').submit(function(evt){
            $.post($(this).attr('action'),$(this).serialize(),function(data, textStatus, jqXHR){
                if(data.max_feedback_reached){
                    $("#answer_"+data.id_question).find('.question-feedback').remove()
                    $("#answer_"+data.id_question).find('.feedback-toggle').html(
                        gettext("You have reached the maximum submitions limit. Please try again later."));
                } else if (data.errors.length){
                    //console.log(data.errors);
                }else {
                    $("#answer_"+data.id_question).find('.question-feedback').remove()
                    $("#answer_"+data.id_question).find('.feedback-toggle').html(
                        gettext("Thanks for helping make this site better. Our team will evaluate your feedback shortly."));
                }
            },'json');
            evt.preventDefault();
        });
        /** Acordeon */
        var settings2 = {
            header: '.question_wrapper',
            active:false
        }
        $('.faqWrapper').accordion(settings2);
    });
}(jQuery));