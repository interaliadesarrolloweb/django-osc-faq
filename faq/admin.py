from django.conf import settings
from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from .abstract import Restricted
from .models import Question, Topic, QuestionFeedback, FAQConfig
from .forms import QuestionForm


class TopicAdmin(TranslationAdmin):
    prepopulated_fields = {'slug_es': ('name_es',), 'slug_en': ('name_en',)}
    list_display = ("name", "active")

admin.site.register(Topic, TopicAdmin)


class QuestionFeedbackInlineAdmin(admin.TabularInline):
    model = QuestionFeedback
    extra = 0
    max_num = 0
    readonly_fields = ('name', 'email', 'is_useful', 'reason', 'created_at')


class QuestionAdmin(TranslationAdmin):
    form = QuestionForm
    list_display = ('text', 'topic', 'feedback_count', 'active', 'order')
    list_editable = ('order', 'active')
    list_filter = ('topic', 'active',)
    search_fields = ('text', 'answer',)
    inlines = (QuestionFeedbackInlineAdmin,)

    class Media:
        css = {
            'all': ('%s%s' % (
                settings.MEDIA_URL,
                '/'.join((
                    'css',
                    'ui-lightness',
                    'jquery-ui-1.8.14.custom.css'
                    )),
            ),),
        }
        js = [
            '%sjs/jquery-1.6.2.min.js' % settings.MEDIA_URL,
            '%sjs/jquery-ui-1.8.14.custom.min.js' % settings.MEDIA_URL,
            '%sjs/sortable_list.js' % settings.MEDIA_URL,
        ]

    def feedback_count(self, instance):
        return instance.feedback.count()

admin.site.register(Question, QuestionAdmin)


class FAQConfigAdmin(Restricted):
    mymodel = FAQConfig

admin.site.register(FAQConfig, FAQConfigAdmin)
