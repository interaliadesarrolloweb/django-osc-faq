# coding: utf-8
from distutils.core import setup

setup(
    name='osc_faq',
    version='0.1',
    description='OSC F.A.Q.',
    author='Jony Kalavera',
    author_email='mr.jony@gmail.com',
    url='http://github.com/jonykalavera/osc_faq/',
    packages=['faq'],
)
